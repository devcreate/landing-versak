function initSubscribeValidate(){
    $('#subscribe__form').validate({
        submitHandler: function() {
            let subscribeEmail = $('#subscribe__email').val();
            let subscribeNumber = $('#subscribe__number').val();
            console.log(subscribeEmail);
            console.log(subscribeNumber);
        },
        rules: {
            email: {
                required: false,
                email: true
            }
        },
        messages: {
            email: "Please enter a valid email address"
        }
    });

    let inp = document.querySelector('#subscribe__number');
    inp.addEventListener('keypress', e => {
        // Отменяем ввод не цифр
        if(!/\d/.test(e.key))
            e.preventDefault();
    });
}

$(document).ready(function(){
    initSubscribeValidate();
});

